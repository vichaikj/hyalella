
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from PIL import Image
import csv

def reg_lineaire(fichier,img,i):

	df = pd.read_csv(fichier, sep=",", header = 0)
	modele_reg=LinearRegression()

	x=df['x'].values.reshape(-1,1)
	y=df['y'].values

	y=-y
	modele_reg.fit(x,y)
	u = np.arange(0,600,0.1) #echantillon du tracé
	z=modele_reg.coef_*u + modele_reg.intercept_ #droite de regression

	img = plt.imread(img)
	fig, ax = plt.subplots()
	ax.imshow(img)
	plt.scatter(x,-y,color='r')
	plt.scatter(u,-z,color='b')
	plt.scatter(x,-modele_reg.predict(x),color='y')
	plt.savefig("./exemple/reg_droite"+str(i))

	return modele_reg