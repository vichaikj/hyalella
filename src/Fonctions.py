# -*- coding: utf-8 -*-
"""
Ce script contient uniquement des fonctions
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from PIL import Image
import csv
import cv2  # OpenCV
from os import path
from numpy import ones,vstack
from numpy.linalg import lstsq

"""
Cree un fichier csv avec la liste de points, avec une colonne x et une colonne y

    PARAMETRES:
listPoint: Liste de points creee par pgm2list
csvFile: Nom du fichier csv (.csv)
"""

#a
def listPoint2csv(listPoint, csvFile):
	with open(listPoint, "r") as in_file:
		stripped = (line.strip() for line in in_file)
		lines = (line.split(" ") for line in stripped if line)
		with open(csvFile, 'w') as out_file:
			writer = csv.writer(out_file)
			writer.writerows(lines)

	with open(csvFile, 'r') as f:
		reader = csv.reader(f)
		rows = list(reader)[1:]
	with open(csvFile, 'w') as out_file:
		writer = csv.writer(out_file)
		writer.writerow(('x', 'y'))
		writer.writerows(rows)


"""
Realise une regression polynomiale de degre 2 et retourne les coefficients dans p

    PARAMETRES:
csvFile: Nom du fichier csv (.csv)

    RETURN:
p: Surement une liste de int contenant les coefficients du polynome (A confirmer)
x:Donnees en abscisses
y:Donnees en ordonnees
"""


def coeffPolyCurve2(csvFile):
	df = pd.read_csv(csvFile + ".csv", sep=",", header=0)
	x = df["x"].values
	y = df["y"].values
	# y= [ -i for i in y]
	p = np.polyfit(x, y, 2)  # Last argument is degree of polynomial

	return p, x, y


"""
Cree une superposition du polynome p, de imagePgm, et du graphique x/y et determine la largeur de la crevette
en utilisant l'image imgClose.
Si saveListPoints est égal à 1, on se contente d'enregistrer une liste de points dans un fichier .txt

    PARAMETRES:
p: Liste de int contenant les coefficients du polynome (A confirmer)
output: chemin du fichier png (ou .txt) dans lequel enregistrer le résultat
x:Donnees en abscisses
y:Donnees en ordonnees
OPTIONNEL debutTrace: Abscisse à partir duquel on va tracer le polynome
OPTIONNEL finTrace: Abscisse jusqu'auquel on va tracer le polynome
OPTIONNEL saveListPoints: Si égal à 1, on se contente d'enregistrer une liste de points dans un fichier .txt
OPTIONNEL onlyCourbe: Si égal à 1 et saveListPoints=None, on se contente de tracer la courbe, sans l'image et le squelette
"""


def surimpPoly(p, imagePgm, output, x, y, debutTrace=None, finTrace=None, saveListPoints=None):
	f = np.poly1d(p)
	fig = plt.figure(frameon=False)
	img = plt.imread(imagePgm)
	ImgWidth = img.shape[1]
	ImgHeight = img.shape[0]

	if (debutTrace == None or finTrace == None):
		plage = np.arange(0, ImgWidth - 1)
	else:
		plage = np.arange(debutTrace, finTrace)

	if (saveListPoints == 1):
		np.savetxt(output, np.vstack((plage, f(plage))).T.astype(int), fmt='%i', delimiter=" ")
	else:
		plt.imshow(img, cmap='gray')
		plt.scatter(x, y, color='b', s=2)
		plt.scatter(plage, f(plage), color='r', s=2)
		plt.savefig(output)

	plt.close()


"""
Mirror horizontalement la crevette si ses pattes sont vers le haut

    PARAMETRES:
p: Surement une liste de int contenant les coefficients du polynome (A confirmer)
imagePgmIn: Nom du fichier pgm correspondant a la crevette
imagePgmOut: Nom du fichier pgm dans lequel enregistrer l'image (.pgm)
"""


def mirrorImageSiPattesVersLeHaut(p, imagePgmIn, imagePgmOut):
	if (p[0] < 0):  # Si le terme en x^2 est inferieur a 0, alors la crevette est dans le mauvais sens
		image_obj = Image.open(imagePgmIn)
		rotated_image = image_obj.transpose(Image.FLIP_TOP_BOTTOM)
		rotated_image.save(imagePgmOut)
	else:
		image_obj = Image.open(imagePgmIn)
		image_obj.save(imagePgmOut)
	print("Done")


"""
Retourne les abscisses de la première et dernière colones de l'image à ne pas avoir uniquement des pixels noirs

    PARAMETRES:
imgPourLargeur: Nom du fichier pgm de la crevette (sur fond noir) dont on doit trouver la largeur

    RETURN:
firstCol: abscisse de la 1ère colonne n'ayant pas que des pixels noirs
lastCol: abscisse de la dernière colonne n'ayant pas que des pixels noirs
"""


def getXPremEtDernPixelBlanc(imgPourLargeur):
	img2 = plt.imread(imgPourLargeur)
	myArray_sum = img2.sum(axis=0)
	firstCol = 0
	lastCol = 0
	for idx, val in enumerate(myArray_sum):
		if (firstCol == 0):
			if (val > 0):
				firstCol = idx
		else:
			if (val == 0):
				lastCol = idx
				break

	if (lastCol == 0):
		lastCol = len(myArray_sum) - 1

	return firstCol, lastCol


"""
Crée 4 masques en fonction du poly et du centreX, qui est l'abscisse du centre, et on enregistre les 4 masques

    PARAMETRES:
p: Liste de int contenant les coefficients du polynome (A confirmer)
imagePgm: image .pgm que l'on va utiliser pour créer les masques
outputSansExtension: chemin du fichier pour enregistrer les résultat, ne pas mettre d'extension .pgm à la fin
OPTIONNEL centreX: Abscisse du centre de la division
OPTIONNEL onlyWrimpParts: 
OPTIONNEL surimpWrimp:
OPTIONNEL save_masks:
"""


def createMasksSecondDeg(p, imagePgm, outputSansExtension, centreX, onlyWrimpParts=None, lissClose=None,
                         surimpWrimp=None, save_masks=None):
	f = np.poly1d(p)
	img = plt.imread(imagePgm)
	ImgWidth = img.shape[1]
	ImgHeight = img.shape[0]
	plt.close()

	# image = img.copy()
	image = 255 * np.ones((ImgHeight, ImgWidth, 1), np.uint8)
	# image=np.zeros((ImgHeight,ImgWidth,1),np.uint8)

	white = 255
	black = 0

	x1 = centreX - 1
	y1 = f(x1)
	x2 = centreX + 1
	y2 = f(x2)
	m, c = points_to_line(x1, y1, x2, y2)
	g = np.poly1d([0, -1/m, c])

	for i in range(0, ImgWidth):
		x = i
		if (f(x) - 1 > 0 and f(x) - 1 < ImgHeight):
			y = (f(x) - 1).astype(int)
			image[y, x] = black
		if (f(x) > 0 and f(x) < ImgHeight):
			y = f(x).astype(int)
			image[y, x] = black
		if (f(x) + 1 > 0 and f(x) + 1 < ImgHeight):
			y = (f(x) + 1).astype(int)
			image[y, x] = black

		x_min = 0
		x_max = 0
		y_min = 9999
		y_max = -1
		for i in range(-ImgWidth, ImgWidth):
			if g(i) < y_min:
				x_min = i
				y_min = int(g(i))
			if g(i) > y_max:
				x_max = i
				y_max = int(g(i))

	# image = cv2.line(image, (centreX, 0), (centreX, ImgHeight), black, 3)
	image = cv2.line(image, (centreX + x_min, y_min), (centreX + x_max, y_max), black, 3)

	if (surimpWrimp == 1):
		cv2.imwrite(outputSansExtension + "_surimp_wrimp.pgm", cv2.bitwise_and(img, img, mask=image))

	mask_DQ = image.copy()
	mask_DT = image.copy()
	mask_VQ = image.copy()
	mask_VT = image.copy()
	mask_DQ_DT = image.copy()
	mask_VQ_VT = image.copy()

	# cv2.floodFill(mask_DQ, None, (centreX-10, 0), black)
	cv2.floodFill(mask_DQ, None, (centreX + ImgWidth/8, 0), black)
	cv2.floodFill(mask_DQ, None, (centreX - ImgWidth/8, ImgHeight - 1), black)
	cv2.floodFill(mask_DQ, None, (centreX + ImgWidth/8, ImgHeight - 1), black)

	cv2.floodFill(mask_DT, None, (centreX - ImgWidth/8, 0), black)
	# cv2.floodFill(mask_DT, None, (centreX+ImgWidth/8, 0), black)
	cv2.floodFill(mask_DT, None, (centreX - ImgWidth/8, ImgHeight - 1), black)
	cv2.floodFill(mask_DT, None, (centreX + ImgWidth/8, ImgHeight - 1), black)

	cv2.floodFill(mask_VQ, None, (centreX - ImgWidth/8, 0), black)
	cv2.floodFill(mask_VQ, None, (centreX + ImgWidth/8, 0), black)
	# cv2.floodFill(mask_VQ, None, (centreX-ImgWidth/8, ImgHeight-1), black)
	cv2.floodFill(mask_VQ, None, (centreX + ImgWidth/8, ImgHeight - 1), black)

	cv2.floodFill(mask_VT, None, (centreX - ImgWidth/8, 0), black)
	cv2.floodFill(mask_VT, None, (centreX + ImgWidth/8, 0), black)
	cv2.floodFill(mask_VT, None, (centreX - ImgWidth/8, ImgHeight - 1), black)
	# cv2.floodFill(mask_VT, None, (centreX+ImgWidth/8, ImgHeight-1), black)

	# cv2.floodFill(mask_DQ_DT, None, (centreX-ImgWidth/8, 0), black)
	# cv2.floodFill(mask_DQ_DT, None, (centreX+ImgWidth/8, 0), black)
	cv2.floodFill(mask_DQ_DT, None, (centreX - ImgWidth/8, ImgHeight - 1), black)
	cv2.floodFill(mask_DQ_DT, None, (centreX + ImgWidth/8, ImgHeight - 1), black)

	cv2.floodFill(mask_VQ_VT, None, (centreX - ImgWidth/8, 0), black)
	cv2.floodFill(mask_VQ_VT, None, (centreX + ImgWidth/8, 0), black)
	# cv2.floodFill(mask_VQ_VT, None, (centreX-ImgWidth/8, ImgHeight-1), black)
	# cv2.floodFill(mask_VQ_VT, None, (centreX+ImgWidth/8, ImgHeight-1), black)

	if (lissClose != None):
		imgLissClose = plt.imread(lissClose)
		mask_DQ_FULL = cv2.bitwise_and(imgLissClose, imgLissClose, mask=mask_DQ)
		mask_DT_FULL = cv2.bitwise_and(imgLissClose, imgLissClose, mask=mask_DT)
		mask_VQ_FULL = cv2.bitwise_and(imgLissClose, imgLissClose, mask=mask_VQ)
		mask_VT_FULL = cv2.bitwise_and(imgLissClose, imgLissClose, mask=mask_VT)
		mask_DQ_DT_FULL = cv2.bitwise_and(imgLissClose, imgLissClose, mask=mask_DQ_DT)
		mask_VQ_VT_FULL = cv2.bitwise_and(imgLissClose, imgLissClose, mask=mask_VQ_VT)

		if save_masks != None:
			cv2.imwrite(outputSansExtension + "_Full_DQ.pgm", mask_DQ_FULL)
			cv2.imwrite(outputSansExtension + "_Full_DT.pgm", mask_DT_FULL)
			cv2.imwrite(outputSansExtension + "_Full_VQ.pgm", mask_VQ_FULL)
			cv2.imwrite(outputSansExtension + "_Full_VT.pgm", mask_VT_FULL)
			cv2.imwrite(outputSansExtension + "_Full_DQ_DT.pgm", mask_DQ_DT_FULL)
			cv2.imwrite(outputSansExtension + "_Full_VQ_VT.pgm", mask_VQ_VT_FULL)
		if (onlyWrimpParts == 1):
			img_DQ = img.copy()
			img_DT = img.copy()
			img_VQ = img.copy()
			img_VT = img.copy()
			img_DQ_DT = img.copy()
			img_VQ_VT = img.copy()

			ajoutName = "_Final"

			cv2.imwrite(outputSansExtension + ajoutName + "_DQ.pgm", cv2.bitwise_and(img_DQ, img_DQ, mask=mask_DQ_FULL))
			cv2.imwrite(outputSansExtension + ajoutName + "_DT.pgm", cv2.bitwise_and(img_DT, img_DT, mask=mask_DT_FULL))
			cv2.imwrite(outputSansExtension + ajoutName + "_VQ.pgm", cv2.bitwise_and(img_VQ, img_VQ, mask=mask_VQ_FULL))
			cv2.imwrite(outputSansExtension + ajoutName + "_VT.pgm", cv2.bitwise_and(img_VT, img_VT, mask=mask_VT_FULL))
			cv2.imwrite(outputSansExtension + ajoutName + "_DQ_DT.pgm",
			            cv2.bitwise_and(img_DQ_DT, img_DQ_DT, mask=mask_DQ_DT_FULL))
			cv2.imwrite(outputSansExtension + ajoutName + "_VQ_VT.pgm",
			            cv2.bitwise_and(img_VQ_VT, img_VQ_VT, mask=mask_VQ_VT_FULL))
	else:
		cv2.imwrite(outputSansExtension + "_DQ.pgm", mask_DQ)
		cv2.imwrite(outputSansExtension + "_DT.pgm", mask_DT)
		cv2.imwrite(outputSansExtension + "_VQ.pgm", mask_VQ)
		cv2.imwrite(outputSansExtension + "_VT.pgm", mask_VT)
		cv2.imwrite(outputSansExtension + "_DQ_DT.pgm", mask_DQ_DT)
		cv2.imwrite(outputSansExtension + "_VQ_VT.pgm", mask_VQ_VT)


"""
!!! Dépend en partie de la fonction createMasksSecondDeg() (pour les noms des images enregistrées)

    PARAMETRES:
"""


def createHistoEtMoy(imgPGM, mask, outputPNG):
	mask = np.asarray(plt.imread(mask).astype(np.uint8))
	imgWrimp = plt.imread(imgPGM)
	hist = cv2.calcHist([imgWrimp], [0], mask, [256], [0, 255])
	plt.plot(hist)
	plt.savefig(outputPNG)
	plt.close()


"""


    PARAMETRES:
"""

def meanWrimpMasked(imgMaskedPGM, varianceEtEcarType=None):
	img = plt.imread(imgMaskedPGM)
	ImgWidth = img.shape[1]
	ImgHeight = img.shape[0]
	compteurActivPixels = 0
	sumPixel = 0
	moyenne = 0
	ecart = 0
	variance = 0
	tab_pixels_hors_mask = []
	for i in range(0, ImgWidth):
		for j in range(0, ImgHeight):
			pixel = img[j, i]
			if pixel != 0:
				tab_pixels_hors_mask.append(pixel)
				sumPixel += pixel
				compteurActivPixels += 1
	if varianceEtEcarType == 1:
		variance = np.var(tab_pixels_hors_mask)
		ecart = np.std(tab_pixels_hors_mask)
		if compteurActivPixels > 0:
			moyenne = sumPixel / compteurActivPixels
	# print(sumPixel/compteurActivPixels)
	return moyenne, variance, ecart


"""


    PARAMETRES:
"""

def convert_img_to_pgm(imgToConvert, outputSansExtension):
	if path.exists(imgToConvert):
		img = plt.imread(imgToConvert)
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		cv2.imwrite(outputSansExtension + '.pgm', gray)
	else:
		print("convert_img_to_pgm() => Image not found: "+imgToConvert+"------------------")

def points_to_line(x1, y1, x2, y2):
	points = [(x1, y1), (x2, y2)]
	x_coords, y_coords = zip(*points)
	A = vstack([x_coords, ones(len(x_coords))]).T
	m, c = lstsq(A, y_coords, rcond=None)[0]
	return m, c
