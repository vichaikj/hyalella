# -*- coding: utf-8 -*-
"""
Ce script permet de definir un seuil d'otsu à des crevettes
"""

import os
from os import path
import Fonctions


def find_seuil(files, chemin_commun):
	for i in files:
		try:
			path_image_jpg = chemin_commun + "/" + str(i)
			if path.exists(path_image_jpg):
				Fonctions.convert_img_to_pgm(path_image_jpg, chemin_commun + "/pgm/" + i[:-4])
			os.system("seuilOtsu "+chemin_commun+"/pgm/"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/otsu_"+i[:-4]+".pgm")
			os.system("border "+chemin_commun+"/pgm/otsu_"+i[:-4]+".pgm"+" 8 "+chemin_commun+"/pgm/border_"+i[:-4]+".pgm")
			os.system("surimp "+chemin_commun+"/pgm/"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/border_"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/seuil/seuil_"+i[:-4]+".pgm")
		except:
			print("\nfind_seuil() FAILED\n")


def find_rseuil(files, chemin_commun):
	for i in files:
		#print("for find_rseuil Start")
		os.system("seuilOtsu "+chemin_commun+"/pgm/rotation/"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/rotsu_"+i[:-4]+".pgm")
		os.system("border "+chemin_commun+"/pgm/rotsu_"+i[:-4]+".pgm"+" 8 "+chemin_commun+"/pgm/rborder_"+i[:-4]+".pgm")
		os.system("surimp "+chemin_commun+"/pgm/rotation/"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/rborder_"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/rseuil/rseuil_"+i[:-4]+".pgm")
		#print("for find_rseuil Done")

path_fo, dirs_fo, files_fo = next(os.walk('../src/femelles_oeufs'))
path_fso, dirs_fso, files_fso = next(os.walk('../src/femelles_sans_oeufs'))
path_mg, dirs_mg, files_mg = next(os.walk('../src/males_gnathopode'))
path_mgne, dirs_mgne, files_mgne = next(os.walk('../src/males_gnathopode_non_evident'))
path_aoi, dirs_aoi, files_aoi = next(os.walk('../src/all_of_it'))

# path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs/pgm/rotation'))
# path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs/pgm/rotation'))
# path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode/pgm/rotation'))
# path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident/pgm/rotation'))

# path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
# path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
# path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
# path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))


def find_seuil_exe():
	find_seuil(files_fo, "femelles_oeufs")
	find_seuil(files_fso, "femelles_sans_oeufs")
	find_seuil(files_mg, "males_gnathopode")
	find_seuil(files_mgne, "males_gnathopode_non_evident")
	find_seuil(files_aoi, "all_of_it")

def find_seuil_exe_2():
	find_seuil(files_fo, "femelles_oeufs/Etape_2")
	find_seuil(files_fso, "femelles_sans_oeufs/Etape_2")
	find_seuil(files_mg, "males_gnathopode/Etape_2")
	find_seuil(files_mgne, "males_gnathopode_non_evident/Etape_2")
	find_seuil(files_aoi, "all_of_it/Etape_2")


def find_rseuil_exe():
	find_rseuil(files_fo, "femelles_oeufs")
	find_rseuil(files_fso, "femelles_sans_oeufs")
	find_rseuil(files_mg, "males_gnathopode")
	find_rseuil(files_mgne, "males_gnathopode_non_evident")
	find_rseuil(files_aoi, "all_of_it")
