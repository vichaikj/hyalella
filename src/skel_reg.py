# -*- coding: utf-8 -*-
"""
Ce script nous permet créer la regression linéaire d'une crevette
"""

import os
import regression

def find_skelreg(files, chemin_commun):
	for i in files:
		try:
			os.system("pgm2list "+chemin_commun+"/pgm/skelcurv_fil_seuil_" + i[:-4] + " b "+chemin_commun+"/pgm/coord_" + i[:-4] + ".csv")
			regression.linear_regression(chemin_commun+"/"+i, chemin_commun+"/pgm/coord_" + i[:-4] + ".csv",
			                             chemin_commun+"/pgm/droite_" + i[:-4] + ".txt",
			                             chemin_commun+"/pgm/skel_reg/skel_reg_" + i[:-4] + ".jpg")
		except:
			print("\nfind_rskelreg() FAILED\n")


def find_rskelreg(files, chemin_commun):
	for i in files:
		try:
			os.system("pgm2list "+chemin_commun+"/pgm/rskelcurv_fil_seuil_" + i[:-4] + " b "+chemin_commun+"/pgm/rcoord_" + i[:-4] + ".csv")
			regression.linear_regression(chemin_commun+"/pgm/rotation/"+i, chemin_commun+"/pgm/rcoord_" + i[:-4] + ".csv",
			                             chemin_commun+"/pgm/rdroite_" + i[:-4] + ".txt",
			                             chemin_commun+"/pgm/rskel_reg/skel_reg_" + i[:-4] + ".jpg")
		except:
			print("\nfind_rskelreg() FAILED\n")


path_fo, dirs_fo, files_fo = next(os.walk('../src/femelles_oeufs'))
path_fso, dirs_fso, files_fso = next(os.walk('../src/femelles_sans_oeufs'))
path_mg, dirs_mg, files_mg = next(os.walk('../src/males_gnathopode'))
path_mgne, dirs_mgne, files_mgne = next(os.walk('../src/males_gnathopode_non_evident'))
path_aoi, dirs_aoi, files_aoi = next(os.walk('../src/all_of_it'))


path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs/pgm/rotation'))
path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs/pgm/rotation'))
path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode/pgm/rotation'))
path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident/pgm/rotation'))
path_raoi, dirs_raoi, files_raoi = next(os.walk('../src/all_of_it/pgm/rotation'))


def find_skelreg_exe():
	find_skelreg(files_fo, "femelles_oeufs")
	find_skelreg(files_fso, "femelles_sans_oeufs")
	find_skelreg(files_mg, "males_gnathopode")
	find_skelreg(files_mgne, "males_gnathopode_non_evident")
	find_skelreg(files_aoi, "all_of_it")


def find_rskelreg_exe():
	find_rskelreg(files_rfo, "femelles_oeufs")
	find_rskelreg(files_rfso, "femelles_sans_oeufs")
	find_rskelreg(files_rmg, "males_gnathopode")
	find_rskelreg(files_rmgne, "males_gnathopode_non_evident")
	find_rskelreg(files_raoi, "all_of_it")


def find_skelreg_exe_2():
	find_skelreg(files_fo, "femelles_oeufs/Etape_2")
	find_skelreg(files_fso, "femelles_sans_oeufs/Etape_2")
	find_skelreg(files_mg, "males_gnathopode/Etape_2")
	find_skelreg(files_mgne, "males_gnathopode_non_evident/Etape_2")
	find_skelreg(files_aoi, "all_of_it/Etape_2")