# -*- coding: utf-8 -*-
"""
!!! les images lissclose_*.pgm doivent déjà avoir été créées
Ce script nous permet, a partir d'un squelette skelcurv_fil_, de detecter 
si la crevette a les pattes vers le haut, et si c'est le cas de retourner horizontalement l'image
"""

from Fonctions import listPoint2csv, coeffPolyCurve2, surimpPoly, mirrorImageSiPattesVersLeHaut, surimpPoly, getXPremEtDernPixelBlanc
import os


def mettre_pattes_vers_le_bas_1(files, chemin_commun):
	for i in files:
		try:
			os.system("pgm2list "+chemin_commun+"/pgm/skelcurv_fil_"+i[:-4]+" b "+chemin_commun+"/pgm/listSkel/listSkel_1_"+i[:-4])
			listPoint2csv(chemin_commun+"/pgm/listSkel/listSkel_1_"+i[:-4], chemin_commun+"/pgm/listSkel/csv_1_"+i[:-4]+ ".csv")
			p, x, y = coeffPolyCurve2(chemin_commun+"/pgm/listSkel/csv_1_"+i[:-4])
			# debut, fin = getXPremEtDernPixelBlanc(chemin_commun+"/pgm/"+"lissclose_"+i[:-4])
			# surimpPoly(p, chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/listSkel_1/surimp_"+"0"+i[:-4]+"_long.png", x, y, debutTrace=debut, finTrace=fin)
			surimpPoly(p, chemin_commun + "/pgm/" + i[:-4] + ".pgm", chemin_commun + "/pgm/listSkel/list_poly_second_deg_1_" + "0" + i[:-4], x, y, saveListPoints=1)
			# mirrorImageSiPattesVersLeHaut(p, chemin_commun+"/pgm/rotation/"+i[:-4]+".pgm", chemin_commun + "/pgm/bon_sens/"+i[:-4]+".pgm")
		except:
			print("\nmettre_pattes_vers_le_bas() FAILED\n")


def mettre_pattes_vers_le_bas_2(files, chemin_commun):
	for i in files:
		try:
			os.system("pgm2list "+chemin_commun+"/pgm/rskelcurv_fil_"+i[:-4]+" b "+chemin_commun+"/pgm/listSkel/listSkel_1_"+i[:-4])
			listPoint2csv(chemin_commun+"/pgm/listSkel/listSkel_1_"+i[:-4], chemin_commun+"/pgm/listSkel/csv_1_"+i[:-4]+ ".csv")
			p, x, y = coeffPolyCurve2(chemin_commun+"/pgm/listSkel/csv_1_"+i[:-4])
			debut, fin = getXPremEtDernPixelBlanc(chemin_commun+"/pgm/"+"lissclose_"+i[:-4])
			surimpPoly(p, chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/listSkel/surimp_"+"0"+i[:-4]+"_long.png", x, y, debutTrace=debut, finTrace=fin)
			surimpPoly(p, chemin_commun + "/pgm/" + i[:-4] + ".pgm",
			           chemin_commun + "/pgm/listSkel/rlist_poly_second_deg_1_" + "0" + i[:-4], x, y, debutTrace=debut,
			           finTrace=fin, saveListPoints=1)
			mirrorImageSiPattesVersLeHaut(p, chemin_commun+"/pgm/rotation/"+i[:-4]+".pgm", chemin_commun + "/pgm/bon_sens/"+i[:-4]+".pgm")
		except:
			print("\nmettre_pattes_vers_le_bas() FAILED\n")


path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))
path_raoi, dirs_raoi, files_raoi = next(os.walk('../src/all_of_it'))


def getShrimpAvecPattesVersLeBas_exe_1():
	mettre_pattes_vers_le_bas_1(files_rfo, "femelles_oeufs")
	mettre_pattes_vers_le_bas_1(files_rfso, "femelles_sans_oeufs")
	mettre_pattes_vers_le_bas_1(files_rmg, "males_gnathopode")
	mettre_pattes_vers_le_bas_1(files_rmgne, "males_gnathopode_non_evident")
	mettre_pattes_vers_le_bas_1(files_raoi, "all_of_it")

def getShrimpAvecPattesVersLeBas_exe_2():
	mettre_pattes_vers_le_bas_2(files_rfo, "femelles_oeufs")
	mettre_pattes_vers_le_bas_2(files_rfso, "femelles_sans_oeufs")
	mettre_pattes_vers_le_bas_2(files_rmg, "males_gnathopode")
	mettre_pattes_vers_le_bas_2(files_rmgne, "males_gnathopode_non_evident")
	mettre_pattes_vers_le_bas_2(files_raoi, "all_of_it")
