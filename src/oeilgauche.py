# -*- coding: utf-8 -*-
"""
Fait une symétrie de l'image si l'oeil se trouve à droite
"""

import os
from PIL import Image
import subprocess

if not os.path.isdir("../src/femelles_oeufs"):
	os.system("mkdir ../src/femelles_oeufs")
	os.system("mkdir ../src/femelles_sans_oeufs")
	os.system("mkdir ../src/males_gnathopode")
	os.system("mkdir ../src/males_gnathopode_non_evident")


def find_oeilgauche(files, chemin_commun):
	for i in files:
		try:
			img = Image.open(chemin_commun+"/oeil_files/CC_oeil_"+i[:-4]+".pgm")
			img_h = Image.open(chemin_commun + "/pgm/bon_sens/" +i[:-4]+".pgm")
			centre = img.size[0] / 2
			os.system("barycentre " + chemin_commun + "/oeil_files/CC_oeil_" + i[:-4] + ".pgm 8 " + chemin_commun + "/oeil_files/CC_oeil_" + i[:-4] + ".pgm")
			coord_x = int(subprocess.check_output(["argmax", chemin_commun + "/oeil_files/CC_oeil_" + i[:-4] + ".pgm"]).decode("utf-8").split(" ")[0])

			if coord_x > centre:
				rotated_img = img_h.transpose(Image.FLIP_LEFT_RIGHT)
				rotated_img.save(chemin_commun + "/Etape_2/pgm/" + i[:-4] + ".pgm")
			else:
				img_h.save(chemin_commun + "/Etape_2/pgm/" + i[:-4] + ".pgm")
		except:
			print("\nfind_oeilgauche() FAILED\n")


path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))


def find_oeilgauche_exe():
	find_oeilgauche(files_rfo, "femelles_oeufs")
	find_oeilgauche(files_rfso, "femelles_sans_oeufs")
	find_oeilgauche(files_rmg, "males_gnathopode")
	find_oeilgauche(files_rmgne, "males_gnathopode_non_evident")
