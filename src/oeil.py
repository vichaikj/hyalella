# -*- coding: utf-8 -*-
"""
Ce script permet de definir un seuil d'otsu à des crevettes
"""

import os
import subprocess
import math



seuil_rouge = "30"
seuil_vert = "30"
seuil_bleu = "30"

path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))
# path_raoi, dirs_raoi, files_raoi = next(os.walk('../src/all_of_it'))


def find_oeil(files, chemin_commun):
	for i in files:
		try:
			dico_files = {}

			os.system("convert "+chemin_commun+"/pgm/rotation/" + i[:-4] + ".pgm " + " "+chemin_commun+"/oeil_files/" + i[:-4] + ".ppm")
			os.system("ppm2pgm "+chemin_commun+"/oeil_files/" + i[:-4] + ".ppm "+chemin_commun+"/oeil_files/r_" + i[
			                                                                                                    :-4] + ".pgm "+chemin_commun+"/oeil_files/v_" + i[:-4] + ".pgm "+chemin_commun+"/oeil_files/b_" + i[:-4] + ".pgm")
			os.system("seuil "+chemin_commun+"/oeil_files/r_" + i[:-4] + ".pgm " + seuil_rouge + " "+chemin_commun+"/oeil_files/r_s_" + i[:-4] + ".pgm")
			os.system("seuil "+chemin_commun+"/oeil_files/v_" + i[:-4] + ".pgm " + seuil_vert + " "+chemin_commun+"/oeil_files/v_s_" + i[:-4] + ".pgm")
			os.system("seuil "+chemin_commun+"/oeil_files/b_" + i[:-4] + ".pgm " + seuil_bleu + " "+chemin_commun+"/oeil_files/b_s_" + i[:-4] + ".pgm")
			os.system("min "+chemin_commun+"/oeil_files/r_s_" + i[:-4] + ".pgm "+chemin_commun+"/oeil_files/v_s_" + i[
			                                                                                                      :-4] + ".pgm "+chemin_commun+"/oeil_files/inter_" + i[:-4])
			os.system("min "+chemin_commun+"/oeil_files/inter_" + i[:-4] + " "+chemin_commun+"/oeil_files/b_s_" + i[
			                                                                                                    :-4] + ".pgm "+chemin_commun+"/oeil_files/noir_" + i[:-4])
			os.system(
				"areaclosing "+chemin_commun+"/oeil_files/noir_" + i[:-4] + " 8 90 "+chemin_commun+"/oeil_files/noir_gros_" + i[:-4])
			os.system(
				"areaclosing "+chemin_commun+"/oeil_files/noir_gros_" + i[:-4] + " 8 300 "+chemin_commun+"/oeil_files/noir_tresgros_" +
				i[:-4])
			os.system(
				"diffimages "+chemin_commun+"/oeil_files/noir_tresgros_" + i[:-4] + " "+chemin_commun+"/oeil_files/noir_gros_" + i[:-4] + " "+chemin_commun+"/oeil_files/noir_moyen_" + i[:-4])
			os.system("labelfgd "+chemin_commun+"/oeil_files/noir_moyen_" + i[:-4] + " 8 "+chemin_commun+"/oeil_files/comps_" + i[:-4])

			for j in range(1, 99):
				os.system("seuil "+chemin_commun+"/oeil_files/comps_" + i[:-4] + " " + str(j) + " " + str(j+1) + " "+chemin_commun+"/oeil_files/CC" + str(j) + "_" + i[:-4])
				area = int(subprocess.check_output(["area", chemin_commun+"/oeil_files/CC" + str(j) + "_" + i[:-4]]).decode("utf-8")[:-1])
				if area == 0:
					break
				os.system("area "+chemin_commun+"/oeil_files/CC" + str(j) + "_" + i[:-4] + " "+chemin_commun+"/oeil_files/area" + str(j) + "_" + i[:-4])
				os.system("border "+chemin_commun+"/oeil_files/CC" + str(j) + "_" + i[:-4] + " 8 "+chemin_commun+"/oeil_files/border" + str(j) + "_" + i[:-4])
				os.system("area "+chemin_commun+"/oeil_files/border" + str(j) + "_" + i[:-4] + " "+chemin_commun+"/oeil_files/perimetre" + str(j) + "_" + i[:-4])

				with open(chemin_commun+"/oeil_files/perimetre" + str(j) + "_" + i[:-4], mode="r") as f:
					l = f.readlines()
					perimetre = l[1]

				with open(chemin_commun+"/oeil_files/area" + str(j) + "_" + i[:-4], mode="r") as f:
					l = f.readlines()
					area = l[1]

				compac = (4*math.pi*float(area))/(float(perimetre)**2)
				dico_files[compac] = chemin_commun+"/oeil_files/border" + str(j) + "_" + i[:-4]

			if dico_files:
				os.system("surimp "+chemin_commun+"/pgm/rotation/"+ i[:-4] + ".pgm " +" "+dico_files[max(dico_files)]+" "+chemin_commun+"/pgm/oeil/oeil_"+i[:-4]+".pgm")
				os.system("convert "+dico_files[max(dico_files)]+" "+chemin_commun+"/oeil_files/CC_oeil_"+i[:-4]+".pgm")
		except:
			print("\nfind_oeil() FAILED\n")

def find_oeil_exe():
	find_oeil(files_rfo, "femelles_oeufs")
	find_oeil(files_rfso, "femelles_sans_oeufs")
	find_oeil(files_rmg, "males_gnathopode")
	find_oeil(files_rmgne, "males_gnathopode_non_evident")
	# find_oeil(files_raoi, "all_of_it")