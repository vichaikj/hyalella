# -*- coding: utf-8 -*-
"""
Ce script va faire un surim de toutes les images avec le polynome de 2nd degré 
de leur squelette, celui-ci aura pour largeur la largeur de l'image
"""
from Fonctions import listPoint2csv, coeffPolyCurve2, surimpPoly, mirrorImageSiPattesVersLeHaut, surimpPoly, getXPremEtDernPixelBlanc

import os

chemin_femelles_oeufs = '../src/femelles_oeufs'
chemin_femelles_sans_oeufs = '../src/femelles_sans_oeufs'
chemin_males_gnathopode = '../src/males_gnathopode'
chemin_males_gnathopode_non_evident = '../src/males_gnathopode_non_evident'

path_fo, dirs_fo, files_fo = next(os.walk(chemin_femelles_oeufs))
path_fso, dirs_fso, files_fso = next(os.walk(chemin_femelles_sans_oeufs))
path_mg, dirs_mg, files_mg = next(os.walk(chemin_males_gnathopode))
path_mgne, dirs_mgne, files_mgne = next(os.walk(chemin_males_gnathopode_non_evident))


def getShrimpAvecPattesVersLeBas(files, chemin_commun):
	os.system("mkdir -p "+chemin_commun+"/pgm/listSkel")
	for i in files:
		os.system("pgm2list "+chemin_commun+"/pgm/skelcurv_fil_"+i[0]+" b "+chemin_commun+"/pgm/listSkel/listSkel_1_"+i[0])
		listPoint2csv(chemin_commun+"/pgm/listSkel/listSkel_1_"+i[0], chemin_commun+"/pgm/listSkel/csv_1_"+i[0]+".csv")
		p, x, y = coeffPolyCurve2(chemin_commun+"/pgm/listSkel/csv_1_"+i[0])
		surimpPoly(p, chemin_commun+"/pgm/"+i[0]+".pgm", chemin_commun+"/pgm/listSkel/list_poly_second_deg_"+i[0]+".txt", x, y, saveListPoints=1)

def PattesBasExe():
	getShrimpAvecPattesVersLeBas(files_fo, "femelles_oeufs")
	getShrimpAvecPattesVersLeBas(files_fso, "femelles_sans_oeufs")
	getShrimpAvecPattesVersLeBas(files_mg, "males_gnathopode")
	getShrimpAvecPattesVersLeBas(files_mgne, "males_gnathopode_non_evident")
