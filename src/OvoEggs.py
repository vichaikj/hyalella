import os
import subprocess
import math


path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))

"""
Renvoie OvoEggs qui prend la valeur de 0 ou 1 (0: ne detecte pas d'ovocytes et/ou d'oeufs 1: Detecte les ovocytes et/ou oeufs)
"""

def OvoEggs(files, chemin_commun):
    for i in files:
        s_otsu = int(subprocess.check_output(["seuilOtsu",chemin_commun+'/pgm/'+i[:-4]+".pgm",chemin_commun+"/OvoEggs/"+i[:-4]+"_BasSeuil.pgm"])[-4:-1])
        seuilBas= round(s_otsu*0.3)
        seuilHaut= round(s_otsu*1.1)
        os.system("seuil "+chemin_commun+'/pgm/'+i[:-4]+".pgm "+str(seuilBas)+" "+chemin_commun+"/OvoEggs/"+i[:-4]+"_BasSeuil.pgm")
        os.system("seuil "+chemin_commun+'/pgm/'+i[:-4]+".pgm "+str(seuilHaut)+" "+chemin_commun+"/OvoEggs/"+i[:-4]+"_HautSeuil.pgm")
        os.system("inverse  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_BasSeuil.pgm")
        os.system("inverse  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_HautSeuil.pgm")
        os.system("areaopening  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_BasSeuil.pgm 8 300 "+chemin_commun+"/OvoEggs/"+i[:-4]+"_Opened.pgm")
        os.system("area  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_Opened.pgm  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_Area")
        os.system("area  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_HautSeuil.pgm  "+chemin_commun+"/OvoEggs/"+i[:-4]+"_AreaTot")

        with open(chemin_commun+"/OvoEggs/"+i[:-4]+"_Area") as f:
            lines = f.read().splitlines()
            v=float(lines[1][0:]) 
        

        with open(chemin_commun+"/OvoEggs/"+i[:-4]+"_AreaTot") as f:
            lines = f.read().splitlines()
            vTot=float(lines[1][0:])             
        if v==0 :
            OvoEgg=0
            print(chemin_commun,i[:-4]," 0    0")
        elif vTot!=0:
            OvoEgg=1
            Vol_OvoEggs=((v*100)/vTot)
            print(chemin_commun,i[:-4]," 1   ",Vol_OvoEggs)
    



def OvoEggs_exe():

    OvoEggs(files_rfo, "femelles_oeufs")
    OvoEggs(files_rfso, "femelles_sans_oeufs")
    OvoEggs(files_rmg, "males_gnathopode")
    OvoEggs(files_rmgne, "males_gnathopode_non_evident")


# OvoEggs_exe()