# -*- coding: utf-8 -*-
"""
Ce script nous permet de definir un squelette a partir d'un seuillage de crevette
"""

import os

def find_skel(files, chemin_commun):
	for i in files:
		try:
			os.system("inverse "+chemin_commun+"/pgm/otsu_"+i[:-4]+".pgm "+chemin_commun+"/pgm/inv_"+i[:-4])
			os.system("areaopening "+chemin_commun+"/pgm/inv_"+i[:-4]+" 8 500 "+chemin_commun+"/pgm/open_"+i[:-4])
			os.system("areaclosing "+chemin_commun+"/pgm/open_"+i[:-4]+" 4 300 "+chemin_commun+"/pgm/close_"+i[:-4])
			os.system("closeball "+chemin_commun+"/pgm/close_"+i[:-4]+" 5 0 "+chemin_commun+"/pgm/liss_"+i[:-4])
			os.system("areaclosing "+chemin_commun+"/pgm/liss_"+i[:-4]+" 4 900 "+chemin_commun+"/pgm/lissclose_"+i[:-4])
			os.system("skelcurv "+chemin_commun+"/pgm/lissclose_"+i[:-4]+" 0 8 "+chemin_commun+"/pgm/skel_"+i[:-4])
			os.system("pgm2skel "+chemin_commun+"/pgm/skel_"+i[:-4]+" 8 100 "+chemin_commun+"/pgm/skel_"+i[:-4]+".skel")
			os.system("skel2pgm "+chemin_commun+"/pgm/skel_"+i[:-4]+".skel "+chemin_commun+"/pgm/skelcurv_fil_"+i[:-4])
			os.system("seuil "+chemin_commun+"/pgm/skelcurv_fil_"+i[:-4]+" 1 "+chemin_commun+"/pgm/skelcurv_fil_seuil_"+i[:-4])
			os.system("dilatball "+chemin_commun+"/pgm/skelcurv_fil_seuil_"+i[:-4]+" 1 0 "+chemin_commun+"/pgm/skelcurv_dil_"+i[:-4])
			os.system("surimp "+chemin_commun+"/pgm/"+i[:-4]+".pgm "+chemin_commun+"/pgm/skelcurv_dil_"+i[:-4]+" "+chemin_commun+"/pgm/skel/skelsur_"+i[:-4]+".pgm")
		except:
			print("\nfind_skel() FAILED\n")


def find_rskel(files, chemin_commun):

	for i in files:
		try:
			os.system("inverse "+chemin_commun+"/pgm/rotsu_"+i[:-4]+".pgm "+chemin_commun+"/pgm/rinv_"+i[:-4])
			os.system("areaopening "+chemin_commun+"/pgm/rinv_"+i[:-4]+" 8 500 "+chemin_commun+"/pgm/ropen_"+i[:-4])
			os.system("areaclosing "+chemin_commun+"/pgm/ropen_"+i[:-4]+" 4 300 "+chemin_commun+"/pgm/rclose_"+i[:-4])
			os.system("closeball "+chemin_commun+"/pgm/rclose_"+i[:-4]+" 5 0 "+chemin_commun+"/pgm/rliss_"+i[:-4])
			os.system("areaclosing "+chemin_commun+"/pgm/rliss_"+i[:-4]+" 4 900 "+chemin_commun+"/pgm/rlissclose_"+i[:-4])
			os.system("skelcurv "+chemin_commun+"/pgm/rlissclose_"+i[:-4]+" 0 8 "+chemin_commun+"/pgm/rskel_"+i[:-4])
			os.system("pgm2skel "+chemin_commun+"/pgm/rskel_"+i[:-4]+" 8 100 "+chemin_commun+"/pgm/rskel_"+i[:-4]+".skel")
			os.system("skel2pgm "+chemin_commun+"/pgm/rskel_"+i[:-4]+".skel "+chemin_commun+"/pgm/rskelcurv_fil_"+i[:-4])
			os.system("seuil "+chemin_commun+"/pgm/rskelcurv_fil_"+i[:-4]+" 1 "+chemin_commun+"/pgm/rskelcurv_fil_seuil_"+i[:-4])
			os.system("dilatball "+chemin_commun+"/pgm/rskelcurv_fil_seuil_"+i[:-4]+" 1 0 "+chemin_commun+"/pgm/rskelcurv_dil_"+i[:-4])
			os.system("surimp "+chemin_commun+"/pgm/rotation/"+i[:-4]+".pgm"+" "+chemin_commun+"/pgm/rskelcurv_dil_"+i[:-4]+" "+chemin_commun+"/pgm/rskel/rskelsur_"+i[:-4]+".pgm")
		except:
			print("\nfind_rskel() FAILED\n")


path_fo, dirs_fo, files_fo = next(os.walk('../src/femelles_oeufs'))
path_fso, dirs_fso, files_fso = next(os.walk('../src/femelles_sans_oeufs'))
path_mg, dirs_mg, files_mg = next(os.walk('../src/males_gnathopode'))
path_mgne, dirs_mgne, files_mgne = next(os.walk('../src/males_gnathopode_non_evident'))
path_aoi, dirs_aoi, files_aoi = next(os.walk('../src/all_of_it'))

# path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs/pgm/rotation'))
# path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs/pgm/rotation'))
# path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode/pgm/rotation'))
# path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident/pgm/rotation'))

# path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
# path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
# path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
# path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))

def find_skel_exe():
	find_skel(files_fo, "femelles_oeufs")
	find_skel(files_fso, "femelles_sans_oeufs")
	find_skel(files_mg, "males_gnathopode")
	find_skel(files_mgne, "males_gnathopode_non_evident")
	find_skel(files_aoi, "all_of_it")

def find_rskel_exe():
	find_rskel(files_fo, "femelles_oeufs")
	find_rskel(files_fso, "femelles_sans_oeufs")
	find_rskel(files_mg, "males_gnathopode")
	find_rskel(files_mgne, "males_gnathopode_non_evident")
	find_rskel(files_aoi, "all_of_it")

def find_skel_exe_2():
	find_skel(files_fo, "femelles_oeufs/Etape_2")
	find_skel(files_fso, "femelles_sans_oeufs/Etape_2")
	find_skel(files_mg, "males_gnathopode/Etape_2")
	find_skel(files_mgne, "males_gnathopode_non_evident/Etape_2")
	find_skel(files_aoi, "all_of_it/Etape_2")