
import os
import subprocess
import math

chemin_femelles_oeufs = '../src/femelles_oeufs'
chemin_femelles_sans_oeufs = '../src/femelles_sans_oeufs'
chemin_males_gnathopode = '../src/males_gnathopode'
chemin_males_gnathopode_non_evident = '../src/males_gnathopode_non_evident'

path_fo, dirs_fo, files_fo = next(os.walk(chemin_femelles_oeufs))
path_fso, dirs_fso, files_fso = next(os.walk(chemin_femelles_sans_oeufs))
path_mg, dirs_mg, files_mg = next(os.walk(chemin_males_gnathopode))
path_mgne, dirs_mgne, files_mgne = next(os.walk(chemin_males_gnathopode_non_evident))

#Pics = '/home/allamenthe/Pictures/Invserse/Pics'




#path_pics, dirs_pics,files_pics = next(os.walk(Pics)) #recupere le nom des fichiers dans le dossier courant 

def find_right_direction():

	res=[[]]

	for i in files_fo:

		os.system("cd femelles_oeufs")
		os.system("convert femelles_oeufs/"+str(i)+" femelles_oeufs/pgm/"+i[0]+".pgm")
		with open("femelles_oeufs/pgm/droite_"+i[0]+".txt") as f: #coef de la droite
			lines = f.read().splitlines()
			p=float(lines[0][0:6])
			p=math.atan(p)/(math.pi /180)

		os.system("seuilOtsu femelles_oeufs/pgm/"+ i[0]+".pgm femelles_oeufs/pgm/otsu_"+i[0]+".pgm")
		#valeur du seuil Otsu
		s_otsu = int(subprocess.check_output(["seuilOtsu","femelles_oeufs/pgm/"+ i[0]+".pgm","femelles_oeufs/pgm/otsu_"+i[0]+".pgm"])[-4:-1])
		seuil30= round(s_otsu*1.2)
		os.system("seuil femelles_oeufs/pgm/" + i[0]+".pgm "+str(seuil30)+" femelles_oeufs/pgm/Otsu+30_"+i[0]+".pgm")

		#inverser les deuximages seuillées
		os.system("inverse femelles_oeufs/pgm/Otsu+30_"+i[0]+".pgm femelles_oeufs/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse femelles_oeufs/pgm/otsu_"+i[0]+".pgm femelles_oeufs/pgm/Otsu_"+i[0]+".pgm")
		#quasiqhear for both inversed pics#######
		os.system("quasishear femelles_oeufs/pgm/Otsu+30_"+ i[0]+".pgm " +str(p)+" femelles_oeufs/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("quasishear femelles_oeufs/pgm/Otsu_"+ i[0]+".pgm " +str(p)+" femelles_oeufs/pgm/Otsu_"+i[0]+".pgm")


		os.system("geodilat femelles_oeufs/pgm/Otsu_"+i[0]+".pgm femelles_oeufs/pgm/Otsu+30_"+i[0]+".pgm  8  -1 femelles_oeufs/pgm/geodilat_"+i[0]+".pgm")

		#Trouver barycentre des deux images
		os.system("byte2long femelles_oeufs/pgm/geodilat_"+i[0]+".pgm femelles_oeufs/pgm/long3_"+i[0]+".pgm")
		os.system("byte2long femelles_oeufs/pgm/Otsu_"+i[0]+".pgm femelles_oeufs/pgm/long_"+i[0]+".pgm" )
		os.system("barycentrelab femelles_oeufs/pgm/long_"+i[0]+".pgm femelles_oeufs/pgm/BaryOtsu_"+i[0]+".pgm")
		os.system("barycentrelab femelles_oeufs/pgm/long3_"+i[0]+".pgm femelles_oeufs/pgm/BaryOtsu+30_"+i[0]+".pgm")

		os.system("pgm2list femelles_oeufs/pgm/BaryOtsu_"+i[0]+".pgm b  femelles_oeufs/pgm/BaryOtsu_"+i[0]+".list")
		os.system("pgm2list femelles_oeufs/pgm/BaryOtsu+30_"+i[0]+".pgm b  femelles_oeufs/pgm/BaryOtsu+30_"+i[0]+".list")

		#quasishear sur l'image d'origine
		os.system("quasishear femelles_oeufs/pgm/"+ i[0]+".pgm " +str(p)+" femelles_oeufs/pgm/Redressement/"+i[0]+".pgm")
		#lire les coordonnées du Barycentre, seulement celle de l'axe y est utilisée 
		with open("femelles_oeufs/pgm/BaryOtsu_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			a=int(lines[1][4:7])
		with open("femelles_oeufs/pgm/BaryOtsu+30_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			b=int(lines[1][4:7])
			#Comparer la hauteur des deux barycentres
		d=a-b
		if d >0 :
			os.system("sym femelles_oeufs/pgm/Redressement/"+i[0]+".pgm"+" v femelles_oeufs/pgm/RightDirection/"+i[0]+".pgm")
			res=res+[[i,"inversée"]]

		else:
			res=res+[[i,"Non inversée"]]
			os.system("cp femelles_oeufs/pgm/Redressement/"+i[0]+".pgm"+" femelles_oeufs/pgm/RightDirection/"+i[0]+".pgm")

		#redresser les images avec la droite de regression dessu
		#os.system("quasishear femelles_oeufs/pgm/skel_reg_"+ i[0]+".pgm " +str(p)+" femelles_oeufs/pgm/Redressement/d"+i[0]+".pgm")    

	##################################

	for i in files_fso:

		os.system("cd femelles_sans_oeufs")
		os.system("convert femelles_sans_oeufs/"+str(i)+" femelles_sans_oeufs/pgm/"+i[0]+".pgm")
		with open("femelles_sans_oeufs/pgm/droite_"+i[0]+".txt") as f:
			lines = f.read().splitlines()
			p=float(lines[0][0:6])
			rad=math.atan(p)
			p=rad/(math.pi /180)

		os.system("seuilOtsu femelles_sans_oeufs/pgm/"+ i[0]+".pgm femelles_sans_oeufs/pgm/otsu_"+i[0]+".pgm")

		s_otsu = int(subprocess.check_output(["seuilOtsu","femelles_sans_oeufs/pgm/"+ i[0]+".pgm","femelles_sans_oeufs/pgm/otsu_"+i[0]+".pgm"])[-4:-1])

		seuil30= round(s_otsu*1.2)
		os.system("seuil femelles_sans_oeufs/pgm/" + i[0]+".pgm "+str(seuil30)+" femelles_sans_oeufs/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse femelles_sans_oeufs/pgm/Otsu+30_"+i[0]+".pgm femelles_sans_oeufs/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse femelles_sans_oeufs/pgm/otsu_"+i[0]+".pgm femelles_sans_oeufs/pgm/Otsu_"+i[0]+".pgm")
		#insert quasiqhear for both inverse#######""
		os.system("quasishear femelles_sans_oeufs/pgm/Otsu+30_"+ i[0]+".pgm " +str(p)+" femelles_sans_oeufs/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("quasishear femelles_sans_oeufs/pgm/Otsu_"+ i[0]+".pgm " +str(p)+" femelles_sans_oeufs/pgm/Otsu_"+i[0]+".pgm")



		os.system("geodilat femelles_sans_oeufs/pgm/Otsu_"+i[0]+".pgm femelles_sans_oeufs/pgm/Otsu+30_"+i[0]+".pgm  8  -1 femelles_sans_oeufs/pgm/geodilat_"+i[0]+".pgm")

		os.system("byte2long femelles_sans_oeufs/pgm/geodilat_"+i[0]+".pgm femelles_sans_oeufs/pgm/long3_"+i[0]+".pgm")
		os.system("byte2long femelles_sans_oeufs/pgm/Otsu_"+i[0]+".pgm femelles_sans_oeufs/pgm/long_"+i[0]+".pgm" )
		os.system("barycentrelab femelles_sans_oeufs/pgm/long_"+i[0]+".pgm femelles_sans_oeufs/pgm/BaryOtsu_"+i[0]+".pgm")
		os.system("barycentrelab femelles_sans_oeufs/pgm/long3_"+i[0]+".pgm femelles_sans_oeufs/pgm/BaryOtsu+30_"+i[0]+".pgm")

		os.system("pgm2list femelles_sans_oeufs/pgm/BaryOtsu_"+i[0]+".pgm b  femelles_sans_oeufs/pgm/BaryOtsu_"+i[0]+".list")
		os.system("pgm2list femelles_sans_oeufs/pgm/BaryOtsu+30_"+i[0]+".pgm b  femelles_sans_oeufs/pgm/BaryOtsu+30_"+i[0]+".list")

		os.system("quasishear femelles_sans_oeufs/pgm/"+ i[0]+".pgm " +str(p)+" femelles_sans_oeufs/pgm/Redressement/"+i[0]+".pgm")

		with open("femelles_sans_oeufs/pgm/BaryOtsu_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			a=int(lines[1][4:7])
		with open("femelles_sans_oeufs/pgm/BaryOtsu+30_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			b=int(lines[1][4:7])

		d=a-b
		if d >0 :
			os.system("sym femelles_sans_oeufs/pgm/Redressement/"+i[0]+".pgm"+" v femelles_sans_oeufs/pgm/RightDirection/"+i[0]+".pgm")
			res=res+[[i,"inversée"]]

		else:
			res=res+[[i,"Non inversée"]]
			os.system("cp femelles_sans_oeufs/pgm/Redressement/"+i[0]+".pgm"+" femelles_sans_oeufs/pgm/RightDirection/"+i[0]+".pgm")
		#os.system("quasishear femelles_sans_oeufs/pgm/skel_reg_"+ i[0]+".pgm " +str(p)+" femelles_sans_oeufs/pgm/Redressement/d"+i[0]+".pgm")  


	###########################"

	for i in files_mg:

		os.system("cd males_gnathopode")
		os.system("convert males_gnathopode/"+str(i)+" males_gnathopode/pgm/"+i[0]+".pgm")
		with open("males_gnathopode/pgm/droite_"+i[0]+".txt") as f:
			lines = f.read().splitlines()
			p=float(lines[0][0:6])
			rad=math.atan(p)
			p=rad/(math.pi /180)

		os.system("seuilOtsu males_gnathopode/pgm/"+ i[0]+".pgm males_gnathopode/pgm/otsu_"+i[0]+".pgm")

		s_otsu = int(subprocess.check_output(["seuilOtsu","males_gnathopode/pgm/"+ i[0]+".pgm","males_gnathopode/pgm/otsu_"+i[0]+".pgm"])[-4:-1])

		seuil30= round(s_otsu*1.2)
		os.system("seuil males_gnathopode/pgm/" + i[0]+".pgm "+str(seuil30)+" males_gnathopode/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse males_gnathopode/pgm/Otsu+30_"+i[0]+".pgm males_gnathopode/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse males_gnathopode/pgm/otsu_"+i[0]+".pgm males_gnathopode/pgm/Otsu_"+i[0]+".pgm")
		#insert quasiqhear for both inverse#######""
		os.system("quasishear males_gnathopode/pgm/Otsu+30_"+ i[0]+".pgm " +str(p)+" males_gnathopode/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("quasishear males_gnathopode/pgm/Otsu_"+ i[0]+".pgm " +str(p)+" males_gnathopode/pgm/Otsu_"+i[0]+".pgm")



		os.system("geodilat males_gnathopode/pgm/Otsu_"+i[0]+".pgm males_gnathopode/pgm/Otsu+30_"+i[0]+".pgm  8  -1 males_gnathopode/pgm/geodilat_"+i[0]+".pgm")

		os.system("byte2long males_gnathopode/pgm/geodilat_"+i[0]+".pgm males_gnathopode/pgm/long3_"+i[0]+".pgm")
		os.system("byte2long males_gnathopode/pgm/Otsu_"+i[0]+".pgm males_gnathopode/pgm/long_"+i[0]+".pgm" )
		os.system("barycentrelab males_gnathopode/pgm/long_"+i[0]+".pgm males_gnathopode/pgm/BaryOtsu_"+i[0]+".pgm")
		os.system("barycentrelab males_gnathopode/pgm/long3_"+i[0]+".pgm males_gnathopode/pgm/BaryOtsu+30_"+i[0]+".pgm")

		os.system("pgm2list males_gnathopode/pgm/BaryOtsu_"+i[0]+".pgm b  males_gnathopode/pgm/BaryOtsu_"+i[0]+".list")
		os.system("pgm2list males_gnathopode/pgm/BaryOtsu+30_"+i[0]+".pgm b  males_gnathopode/pgm/BaryOtsu+30_"+i[0]+".list")

		os.system("quasishear males_gnathopode/pgm/"+ i[0]+".pgm " +str(p)+" males_gnathopode/pgm/Redressement/"+i[0]+".pgm")

		with open("males_gnathopode/pgm/BaryOtsu_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			a=int(lines[1][4:7])
		with open("males_gnathopode/pgm/BaryOtsu+30_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			b=int(lines[1][4:7])

		d=a-b
		if d >0 :
			os.system("sym males_gnathopode/pgm/Redressement/"+i[0]+".pgm"+" v males_gnathopode/pgm/RightDirection/"+i[0]+".pgm")
			res=res+[[i,"inversée"]]

		else:
			res=res+[[i,"Non inversée"]]
			os.system("cp males_gnathopode/pgm/Redressement/"+i[0]+".pgm"+" males_gnathopode/pgm/RightDirection/"+i[0]+".pgm")
		#os.system("quasishear males_gnathopode/pgm/skel_reg_"+ i[0]+".pgm " +str(p)+" males_gnathopode/pgm/Redressement/d"+i[0]+".pgm")
	#########################

	for i in files_mgne:

		os.system("cd males_gnathopode_non_evident")
		os.system("convert males_gnathopode_non_evident/"+str(i)+" males_gnathopode_non_evident/pgm/"+i[0]+".pgm")
		with open("males_gnathopode_non_evident/pgm/droite_"+i[0]+".txt") as f:
			lines = f.read().splitlines()
			p=float(lines[0][0:6])
			rad=math.atan(p)
			p=rad/(math.pi /180)

		os.system("seuilOtsu males_gnathopode_non_evident/pgm/"+ i[0]+".pgm males_gnathopode_non_evident/pgm/otsu_"+i[0]+".pgm")

		s_otsu = int(subprocess.check_output(["seuilOtsu","males_gnathopode_non_evident/pgm/"+ i[0]+".pgm","males_gnathopode_non_evident/pgm/otsu_"+i[0]+".pgm"])[-4:-1])

		seuil30= round(s_otsu*1.2)
		os.system("seuil males_gnathopode_non_evident/pgm/" + i[0]+".pgm "+str(seuil30)+" males_gnathopode_non_evident/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse males_gnathopode_non_evident/pgm/Otsu+30_"+i[0]+".pgm males_gnathopode_non_evident/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("inverse males_gnathopode_non_evident/pgm/otsu_"+i[0]+".pgm males_gnathopode_non_evident/pgm/Otsu_"+i[0]+".pgm")
		#insert quasiqhear for both inverse#######""
		os.system("quasishear males_gnathopode_non_evident/pgm/Otsu+30_"+ i[0]+".pgm " +str(p)+" males_gnathopode_non_evident/pgm/Otsu+30_"+i[0]+".pgm")
		os.system("quasishear males_gnathopode_non_evident/pgm/Otsu_"+ i[0]+".pgm " +str(p)+" males_gnathopode_non_evident/pgm/Otsu_"+i[0]+".pgm")



		os.system("geodilat males_gnathopode_non_evident/pgm/Otsu_"+i[0]+".pgm males_gnathopode_non_evident/pgm/Otsu+30_"+i[0]+".pgm  8  -1 males_gnathopode_non_evident/pgm/geodilat_"+i[0]+".pgm")

		os.system("byte2long males_gnathopode_non_evident/pgm/geodilat_"+i[0]+".pgm males_gnathopode_non_evident/pgm/long3_"+i[0]+".pgm")
		os.system("byte2long males_gnathopode_non_evident/pgm/Otsu_"+i[0]+".pgm males_gnathopode_non_evident/pgm/long_"+i[0]+".pgm" )
		os.system("barycentrelab males_gnathopode_non_evident/pgm/long_"+i[0]+".pgm males_gnathopode_non_evident/pgm/BaryOtsu_"+i[0]+".pgm")
		os.system("barycentrelab males_gnathopode_non_evident/pgm/long3_"+i[0]+".pgm males_gnathopode_non_evident/pgm/BaryOtsu+30_"+i[0]+".pgm")

		os.system("pgm2list males_gnathopode_non_evident/pgm/BaryOtsu_"+i[0]+".pgm b  males_gnathopode_non_evident/pgm/BaryOtsu_"+i[0]+".list")
		os.system("pgm2list males_gnathopode_non_evident/pgm/BaryOtsu+30_"+i[0]+".pgm b  males_gnathopode_non_evident/pgm/BaryOtsu+30_"+i[0]+".list")

		os.system("quasishear males_gnathopode_non_evident/pgm/"+ i[0]+".pgm " +str(p)+" males_gnathopode_non_evident/pgm/Redressement/"+i[0]+".pgm")

		with open("males_gnathopode_non_evident/pgm/BaryOtsu_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			a=int(lines[1][4:7])
		with open("males_gnathopode_non_evident/pgm/BaryOtsu+30_"+i[0]+".list") as f:
			lines = f.read().splitlines()
			b=int(lines[1][4:7])

		d=a-b
		if d >0 :
			os.system("sym males_gnathopode_non_evident/pgm/Redressement/"+i[0]+".pgm"+" v males_gnathopode_non_evident/pgm/RightDirection/"+i[0]+".pgm")
			res=res+[[i,"inversée"]]

		else:
			res=res+[[i,"Non inversée"]]
			os.system("cp males_gnathopode_non_evident/pgm/Redressement/"+i[0]+".pgm"+" males_gnathopode_non_evident/pgm/RightDirection/"+i[0]+".pgm")

		#os.system("quasishear males_gnathopode_non_evident/pgm/skel_reg_"+ i[0]+".pgm " +str(p)+" males_gnathopode_non_evident/pgm/Redressement"+i[0]+".pgm")
