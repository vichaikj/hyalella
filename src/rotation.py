# -*- coding: utf-8 -*-
"""
Ce script nous permet de definir un squelette a partir d'un seuillage de crevette
"""

import os
import math
import numpy as np
from PIL import Image
import Fonctions
import cv2


chemin_femelles_oeufs = '../src/femelles_oeufs'
chemin_femelles_sans_oeufs = '../src/femelles_sans_oeufs'
chemin_males_gnathopode = '../src/males_gnathopode'
chemin_males_gnathopode_non_evident = '../src/males_gnathopode_non_evident'


path_fo, dirs_fo, files_fo = next(os.walk(chemin_femelles_oeufs))
path_fso, dirs_fso, files_fso = next(os.walk(chemin_femelles_sans_oeufs))
path_mg, dirs_mg, files_mg = next(os.walk(chemin_males_gnathopode))
path_mgne, dirs_mgne, files_mgne = next(os.walk(chemin_males_gnathopode_non_evident))
path_aoi, dirs_aoi, files_aoi = next(os.walk('../src/all_of_it'))


def find_teta(files, chemin_commun):
	for i in files:
		coefs = []

		with open(chemin_commun+"/pgm/droite_" + i[:-4] + ".txt") as f:
			lines = f.read().split(" ")
			for elt in lines:
				coefs.append(elt)

		a = float(coefs[0])
		teta_rad = math.atan(a)
		teta_deg = teta_rad / (math.pi / 180)

		with open(chemin_commun+"/pgm/teta_"+i[:-4]+".txt", "w") as f:
			f.write(str(teta_deg))

		Fonctions.convert_img_to_pgm(chemin_commun+"/"+str(i), chemin_commun+"/pgm/"+i[:-4])
		#os.system("convert "+chemin_commun+"/"+str(i)+" "+chemin_commun+"/pgm/"+i[:-4]+".pgm")
		os.system("quasishear "+chemin_commun+"/pgm/"+i[:-4] + ".pgm "+str(teta_deg)+" "+chemin_commun+"/pgm/rotation_"+i[:-4]+".pgm")

		# Remplace les coins noirs de l'image par une couleur grise similaire... méthode surement améliorable
		os.system("seuil "+chemin_commun+"/pgm/rotation_" + i[:-4] + ".pgm 1 "+chemin_commun+"/pgm/rs_"+i[:-4]+".pgm")
		os.system("areaclosing "+chemin_commun+"/pgm/rs_"+i[:-4]+".pgm 8 100 "+chemin_commun+"/pgm/rsclosing_"+i[:-4]+".pgm")
		os.system("inverse "+chemin_commun+"/pgm/rsclosing_"+i[:-4]+".pgm "+chemin_commun+"/pgm/inv_rsc_"+i[:-4]+".pgm")
		os.system("max "+chemin_commun+"/pgm/inv_rsc_"+i[:-4]+".pgm "+chemin_commun+"/pgm/rotation_"+i[:-4]+".pgm "+chemin_commun+"/pgm/mrotation_"+i[:-4]+".pgm")

		im = Image.open(chemin_commun+"/pgm/mrotation_" + i[:-4] + ".pgm")
		im = im.convert("RGBA")

		data = np.array(im)
		red, green, blue, alpha = data.T

		black_areas = (red == 255) & (blue == 255) & (green == 255)
		data[..., :-1][black_areas.T] = (155, 154, 150)

		im2 = Image.fromarray(data)

		# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		im2.save(chemin_commun + "/pgm/a_rogner_" + i[:-4] + ".pgm")

		img2_cv2 = cv2.imread(chemin_commun + "/pgm/a_rogner_" + i[:-4] + ".pgm")
		h = 480
		w = 640
		im2_cv2_crop = img2_cv2[0:h, 0:w] # TEMPORAIRE = crop fait vite fait pour pouvoir executer le main.py
		im2_cv2_crop_gray = cv2.cvtColor(im2_cv2_crop, cv2.COLOR_BGR2GRAY)
		cv2.imwrite(chemin_commun + "/pgm/rotation/" + i[:-4] + ".pgm", im2_cv2_crop_gray)

def find_teta_exe():
	find_teta(files_fo, "femelles_oeufs")
	find_teta(files_fso, "femelles_sans_oeufs")
	find_teta(files_mg, "males_gnathopode")
	find_teta(files_mgne, "males_gnathopode_non_evident")
	find_teta(files_aoi, "all_of_it")
