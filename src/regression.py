"""
Ce script nous permet de creer la regression lineaire d'une crevette
"""

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
#from tkinter import*


def linear_regression(in_image, coord, droite, out_image):

	df = pd.read_csv(coord, sep=" ", header=0)

	df.columns = ["x", "y"]
	modele_reg = LinearRegression()

	x = df["x"].values.reshape(-1, 1)
	y = df["y"].values

	y = -y

	modele_reg.fit(x, y)
	a = str(modele_reg.coef_)[1:-1]
	b = str(modele_reg.intercept_)

	with open(droite, "w") as f:
		f.write(a + " " + b)

	img = plt.imread(in_image)
	fig, ax = plt.subplots()
	ax.imshow(img)

	plt.scatter(x, -y, color='r')
	plt.scatter(x, -modele_reg.predict(x), color='y')
	plt.savefig(out_image)

	plt.close()
