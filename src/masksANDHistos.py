# -*- coding: utf-8 -*-
"""
!!! les images lissclose_*.pgm doivent déjà avoir été créées
Ce script nous permet, a partir d'un squelette skelcurv_fil_ et de l'image pgm de la crevette, de 
découper la crevette en 4 parties en utilisant le polynome de second degré
"""
from Fonctions import listPoint2csv, coeffPolyCurve2, surimpPoly, mirrorImageSiPattesVersLeHaut, surimpPoly, getXPremEtDernPixelBlanc, createMasksSecondDeg, createHistoEtMoy, meanWrimpMasked

import os

chemin_femelles_oeufs = '../src/femelles_oeufs'
chemin_femelles_sans_oeufs = '../src/femelles_sans_oeufs'
chemin_males_gnathopode = '../src/males_gnathopode'
chemin_males_gnathopode_non_evident = '../src/males_gnathopode_non_evident'
chemin_all_of_it = '../src/all_of_it'


path_fo, dirs_fo, files_fo = next(os.walk(chemin_femelles_oeufs))
path_fso, dirs_fso, files_fso = next(os.walk(chemin_femelles_sans_oeufs))
path_mg, dirs_mg, files_mg = next(os.walk(chemin_males_gnathopode))
path_mgne, dirs_mgne, files_mgne = next(os.walk(chemin_males_gnathopode_non_evident))
path_aoi, dirs_aoi, files_aoi = next(os.walk(chemin_all_of_it))


def masks_and_histo( files, chemin_commun, indice, histo=None, mean=None ):
	data = {}
	for i in files:
		try:
			os.system("pgm2list "+chemin_commun+"/pgm/skelcurv_fil_"+i[:-4]+" b "+chemin_commun+"/pgm/divBy4/listSkel_1_"+i[:-4])
			listPoint2csv( chemin_commun+"/pgm/divBy4/listSkel_1_"+i[:-4], chemin_commun+"/pgm/divBy4/csv_1_"+i[:-4]+ ".csv")
			p, x, y = coeffPolyCurve2( chemin_commun+"/pgm/divBy4/csv_1_"+i[:-4])
			debut, fin = getXPremEtDernPixelBlanc( chemin_commun+"/pgm/"+"lissclose_"+i[:-4] )
			print(debut)
			print(fin)
			createMasksSecondDeg(p, chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/divBy4/Masks/mask_"+indice+"_"+i[:-4], debut+(fin-debut)/2, lissClose=chemin_commun+"/pgm/"+"lissclose_"+i[:-4], surimpWrimp=1, save_masks=1)
			createMasksSecondDeg(p, chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4], debut+(fin-debut)/2, lissClose=chemin_commun+"/pgm/"+"lissclose_"+i[:-4], onlyWrimpParts=1)
			if histo == 1:
				createHistoEtMoy(chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/divBy4/Masks/mask_"+indice+"_"+i[:-4]+"_Full_VQ_VT.pgm", chemin_commun+"/pgm/divBy4/Histos/Histo_VQ_VT_"+indice+"_"+i[:-4]+".png")
				createHistoEtMoy(chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/divBy4/Masks/mask_"+indice+"_"+i[:-4]+"_Full_VT.pgm", chemin_commun+"/pgm/divBy4/Histos/Histo_VT_"+indice+"_"+i[:-4]+".png")
				createHistoEtMoy(chemin_commun+"/pgm/"+i[:-4]+".pgm", chemin_commun+"/pgm/divBy4/Masks/mask_"+indice+"_"+i[:-4]+"_Full_DQ_DT.pgm", chemin_commun+"/pgm/divBy4/Histos/Histo_DQ_DT_"+indice+"_"+i[:-4]+".png")

			if mean == 1:
				#print("Mean " + "wrimp_"+indice+"_"+i[:-4]+ " VQ_VT =" + str(meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ_VT.pgm") ) )
				#print("Mean " + "wrimp_"+indice+"_"+i[:-4]+ " VT =" + str(meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VT.pgm") ) )
				#print("Mean " + "wrimp_"+indice+"_"+i[:-4]+ " DQ_DT =" + str(meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DQ_DT.pgm") ) )

				d={"VQ_VT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ_VT.pgm") ,
					"VT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VT.pgm"),
				    "DQ_DT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DQ_DT.pgm"),
				   "VQ":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ.pgm"),
				   "DT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DT.pgm"),
				   "DQ":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DQ.pgm")
				}
				data.update({"wrimp_"+indice+"_"+i[:-4]+"":d})
		except:
			print("\nmasks_and_histo() FAILED\n")
	if mean == 1 :
		return data



def masksEtHisto_exe_2():
	masks_and_histo( files_fo, "femelles_oeufs/Etape_2", "0", histo=1, mean=1 )
	masks_and_histo( files_fso, "femelles_sans_oeufs/Etape_2", "1", histo=1 )
	masks_and_histo( files_mg, "males_gnathopode/Etape_2", "2", histo=1 )
	masks_and_histo( files_mgne, "males_gnathopode_non_evident/Etape_2", "3", histo=1 )
	masks_and_histo(files_aoi, "all_of_it/Etape_2", "3", histo=1)

def created_data( files, chemin_commun, indice, histo=None, mean=None):
	data = {}
	for i in files:
		# print(i)
		# print(chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ_VT.pgm\n")
		try:
			if mean == 1:
						#print("Mean " + "wrimp_"+indice+"_"+i[:-4]+ " VQ_VT =" + str(meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ_VT.pgm") ) )
						#print("Mean " + "wrimp_"+indice+"_"+i[:-4]+ " VT =" + str(meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VT.pgm") ) )
						#print("Mean " + "wrimp_"+indice+"_"+i[:-4]+ " DQ_DT =" + str(meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DQ_DT.pgm") ) )
				d={"Nom":i[:-4],
					"VQ_VT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ_VT.pgm",1)[0],
					"VT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VT.pgm",1)[0],
					"DQ_DT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DQ_DT.pgm",1)[0],
					"VQ":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VQ.pgm",1)[0],
					"DT":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DT.pgm",1)[0],
					"DQ":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_DQ.pgm",1)[0],
				   "Ecart_type":meanWrimpMasked( chemin_commun+"/pgm/divBy4/WrimpDivided/wrimp_"+indice+"_"+i[:-4]+"_Final_VT.pgm",1)[2]
				}
			data.update({"wrimp_"+indice+"_"+i[:-4]+"":d})
		except IOError as e:
			print("I/O error({0}): {1}".format(e.errno, e.strerror))
	return data

