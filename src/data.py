# -*- coding: utf-8 -*-

#male represente par un  "1"
#femelle par zero "0"


import csv
import masksANDHistos
from masksANDHistos import masks_and_histo , created_data
import os

chemin_femelles_oeufs = '../src/femelles_oeufs'
chemin_femelles_sans_oeufs = '../src/femelles_sans_oeufs'
chemin_males_gnathopode = '../src/males_gnathopode'
chemin_males_gnathopode_non_evident = '../src/males_gnathopode_non_evident'
chemin_all_of_it = '../src/all_of_it'


path_fo, dirs_fo, files_fo = next(os.walk(chemin_femelles_oeufs))
path_fso, dirs_fso, files_fso = next(os.walk(chemin_femelles_sans_oeufs))
path_mg, dirs_mg, files_mg = next(os.walk(chemin_males_gnathopode))
path_mgne, dirs_mgne, files_mgne = next(os.walk(chemin_males_gnathopode_non_evident))
path_aoi, dirs_aoi, files_aoi = next(os.walk(chemin_all_of_it))



# feature = {img : [diff_VQ_VT,diff DQ_DT , diff_VT_DT , diff_DQ_VQ ,species]}


def data_femelle_male(chemin, chemin_2, indice, sex, mean=1, o_os=0):
	feature={} # femelle  = 0 # male  = 1 #  oeuf = 1 # sans oeuf  = 0
	data=created_data(chemin_2,chemin,indice,mean=1)
	for i in data.keys():
		try:
			if(data[i]["VQ"]!=0 and data[i]["VT"]!=0 and data[i]["DT"]!=0):
				d=[float(data[i]["VQ"])/float(data[i]["VT"]),  #diff_VQ_VT : difference entre les deux extreminte bas (ventre-queueu , ventre tete)
				   float(data[i]["DQ"])/float(data[i]["DT"]),  #diff DQ_DT: difference entre les deux extremites haut (dos queue , dos tete)
				   float(data[i]["VT"])/float(data[i]["DT"]),  #diff_VT_DT: difference entre le bas et le haut de la tete
				   float(data[i]["DQ"])/float(data[i]["VQ"]),  #diff_DQ_VQ: diffence entre le bas et le haut de la queu
				   float(data[i]["DQ_DT"]),
				   float(data[i]["VQ_VT"]),
				   data[i]["Ecart_type"],
				   o_os,
				   sex,
				   data[i]['Nom']
				   ]
				feature.update({i:d})
			else:
				raise ZeroDivisionError("division par zero")
		except ZeroDivisionError:
			print("division par zero")
	return feature;

femelle=data_femelle_male("femelles_oeufs/Etape_2",files_fo,"0",0,1,1)
# data_femelle_male("femelles_sans_oeufs/Etape_2",files_fso,"1",0,1)
male=data_femelle_male("males_gnathopode/Etape_2",files_mg,"2",1,1,0)
#male=data_femelle_male("males_gnathopode_non_evident/Etape_2",files_mgne,"3",1,1,0)

#male gnatope non evident

#male_n =data_femelle_male("males_gnathopode_non_evident/Etape_2",files_mgne,"3",3,1)

#femmelle sans oeuf

femelle_o =data_femelle_male("femelles_sans_oeufs/Etape_2",files_fso,"1",0,1,0)








# data=created_data(files_fso,"femelles_sans_oeufs/Etape_2", "1",mean=1)

# for i in data.keys():
# 	try:
# 		if(data[i]["VQ"]!=0 and data[i]["VT"]!=0 and data[i]["DT"]!=0):
# 			d=[float(data[i]["VQ"])/float(data[i]["VT"]),  #diff_VQ_VT : difference entre les deux extreminte bas (ventre-queueu , ventre tete)
# 			   float(data[i]["DQ"])/float(data[i]["DT"]),  #diff DQ_DT: difference entre les deux extremites haut (dos queue , dos tete)
# 			   float(data[i]["VT"])/float(data[i]["DT"]),  #diff_VT_DT: difference entre le bas et le haut de la tete
# 			   float(data[i]["DQ"])/float(data[i]["VQ"]),  #diff_DQ_VQ: diffence entre le bas et le haut de la queu
# 			   data[i]["Ecart_type"],
# 			   0
# 			   ]
# 			femelle.update({i:d})
# 		else:
# 			raise ZeroDivisionError("division par zero")
# 	except ZeroDivisionError:
# 		print("division par zero")

# data=created_data(files_mg,"males_gnathopode/Etape_2", "2",mean=1)
# for i in data.keys():
# 	try:
# 		if (data[i]["VQ"] != 0 and data[i]["VT"] != 0 and data[i]["DT"] != 0):
# 			d=[float(data[i]["VQ"])/float(data[i]["VT"]),  #diff_VQ_VT : difference entre les deux extreminte bas (ventre-queueu , ventre tete)
# 			   float(data[i]["DQ"])/float(data[i]["DT"]),  #diff DQ_DT: difference entre les deux extremites haut (dos queue , dos tete)
# 			   float(data[i]["VT"])/float(data[i]["DT"]),  #diff_VT_DT: difference entre le bas et le haut de la tete
# 			   float(data[i]["DQ"])/float(data[i]["VQ"]),  #diff_DQ_VQ: diffence entre le bas et le haut de la queu
# 			   data[i]["Ecart_type"],
# 			   1
# 			   ]
# 			male.update({i:d})
# 		else:
# 			raise ZeroDivisionError("division par zero")
# 	except ZeroDivisionError:
# 		print("division par zero")

# data=created_data(files_mgne,"males_gnathopode_non_evident/Etape_2", "3",mean=1)
# for i in data.keys():
# 		try:
# 			if (data[i]["VQ"] != 0 and data[i]["VT"] != 0 and data[i]["DT"] != 0):
# 				d=[float(data[i]["VQ"])/float(data[i]["VT"]),  #diff_VQ_VT : difference entre les deux extreminte bas (ventre-queueu , ventre tete)
# 				   float(data[i]["DQ"])/float(data[i]["DT"]),  #diff DQ_DT: difference entre les deux extremites haut (dos queue , dos tete)
# 				   float(data[i]["VT"])/float(data[i]["DT"]),  #diff_VT_DT: difference entre le bas et le haut de la tete
# 				   float(data[i]["DQ"])/float(data[i]["VQ"]),  #diff_DQ_VQ: diffence entre le bas et le haut de la queu
# 				   data[i]["Ecart_type"],
# 				   1
# 				   ]
# 				male.update({i:d})
# 			else:
# 				raise ZeroDivisionError("division par zero")
# 		except ZeroDivisionError:
# 			print("division par zero")

# data=created_data(files_fo,"femelles_oeufs/Etape_2", "0",mean=1)

# for i in data.keys():
# 	try:
# 		if (data[i]["VQ"] != 0 and data[i]["VT"] != 0 and data[i]["DT"] != 0):
# 			d=[float(data[i]["VQ"])/float(data[i]["VT"]),  #diff_VQ_VT : difference entre les deux extreminte bas (ventre-queueu , ventre tete)
# 			   float(data[i]["DQ"])/float(data[i]["DT"]),  #diff DQ_DT: difference entre les deux extremites haut (dos queue , dos tete)
# 			   float(data[i]["VT"])/float(data[i]["DT"]),  #diff_VT_DT: difference entre le bas et le haut de la tete
# 			   float(data[i]["DQ"])/float(data[i]["VQ"]),  #diff_DQ_VQ: diffence entre le bas et le haut de la queu
# 			   data[i]["Ecart_type"],
# 			   0
# 			   ]
# 			femelle.update({i:d})
# 		else:
# 			raise ZeroDivisionError("division par zero")
# 	except ZeroDivisionError:
# 		print("division par zero")

print(male)
print(femelle)


with open('data/hyalella.csv', 'w', newline='') as csvfile:
	fieldnames = ['Nom','diff_VQ_VT','diff DQ_DT' ,'diff_VT_DT' , 'diff_DQ_VQ','species', 'Ecart_type','mean_VQ_VT','mean_DQ_DT','o_so']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()
	for elt in femelle_o:
		writer.writerow(
			{'Nom':femelle_o[elt][9],'diff_VQ_VT':femelle_o[elt][0],'diff DQ_DT':femelle_o[elt][1] ,
			 'diff_VT_DT':femelle_o[elt][2] , 'diff_DQ_VQ':femelle_o[elt][3] ,
			 'species':femelle_o[elt][7] , 'Ecart_type' : femelle_o[elt][8],
			 'mean_VQ_VT':femelle_o[elt][5] ,'mean_DQ_DT':femelle_o[elt][4],
			 'o_so':femelle_o[elt][7]})
	for elt in femelle:
		writer.writerow(
			{'Nom':femelle[elt][9],'diff_VQ_VT': femelle[elt][0],
			 'diff_VT_DT': femelle[elt][1],
			 'diff_VT_DT': femelle[elt][2],
			 'diff_DQ_VQ': femelle[elt][3],
			 'species': femelle[elt][8],
			 'Ecart_type': femelle[elt][6],
			 'mean_VQ_VT': femelle[elt][5],
			 'mean_DQ_DT': femelle[elt][4],
			 'o_so':femelle[elt][7]})
	for m in male:
		writer.writerow({'Nom':male[m][9],'diff_VQ_VT': male[m][0],
						 'diff DQ_DT': male[m][1],
						 'diff_VT_DT': male[m][2],
						 'diff_DQ_VQ': male[m][3],
						 'species': male[m][8],
						 'Ecart_type':male[m][6],
						 'mean_VQ_VT':male[m][5] ,
						 'mean_DQ_DT':male[m][4],
						 'o_so':male[m][7]})
	# for m in male_n:
	# 	writer.writerow({'diff_vq_vt': male_n[m][0],
	# 					 'diff dq_dt': male_n[m][1],
	# 					 'diff_vt_dt': male_n[m][2],
	# 					  'diff_dq_vq': male_n[m][3], 
	# 					  'species': male_n[m][8],
	# 					  'ecart_type':male_n[m][6],
	# 					  'mean_vq_vt':male_n[m][5] ,
	# 					  'mean_dq_dt':male_n[m][4],
	# 					  'o_so':male_n[m][7]})