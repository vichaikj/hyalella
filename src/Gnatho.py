# -*- coding: utf-8 -*-


import os
import subprocess
import math
import cv2
import os.path

path_rfo, dirs_rfo, files_rfo = next(os.walk('../src/femelles_oeufs'))
path_rfso, dirs_rfso, files_rfso = next(os.walk('../src/femelles_sans_oeufs'))
path_rmg, dirs_rmg, files_rmg = next(os.walk('../src/males_gnathopode'))
path_rmgne, dirs_rmgne, files_rmgne = next(os.walk('../src/males_gnathopode_non_evident'))

"""
Retourne soit un résultats binaire: Gnathopodes= 0: ne detecte rien 1:detecte peut-être un gnatho
soit une variable quantitative :VolGnatho c'est le % du volume du gnathopode détécté par rapport au masque de la crevette.

/!\ Si l'eoil n'a pas étté trouvé préalablement, la fonction prend la composante la plus volumineuse et non la plus proche de l'oeil !
"""

def Gnatho(files, chemin_commun):

    for i in files:
        try:
        

            s_otsu = int(subprocess.check_output(["seuilOtsu",chemin_commun+'/pgm/rotation/'+i[:-4]+".pgm",chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm"])[-4:-1])
            seuilBas= round(s_otsu*0.8)
            seuilHaut=round(s_otsu*1.1)
            os.system("seuil "+chemin_commun+'/pgm/rotation/'+i[:-4]+".pgm "+str(seuilHaut)+" "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm")
            os.system("seuil "+chemin_commun+'/pgm/rotation/'+i[:-4]+".pgm "+str(seuilBas)+" "+chemin_commun+"/Gnatho/"+i[:-4]+"_BasSeuil.pgm")
            os.system("inverse  "+chemin_commun+"/Gnatho/"+i[:-4]+"_BasSeuil.pgm")
            os.system("inverse  "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm")
            os.system("sub  "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm  "+chemin_commun+"/Gnatho/"+i[:-4]+"_BasSeuil.pgm  "+chemin_commun+"/Gnatho/"+i[:-4]+"_sub.pgm")
            os.system("areaclosing  "+chemin_commun+"/Gnatho/"+i[:-4]+"_sub.pgm  8 50 "+chemin_commun+"/Gnatho/"+i[:-4]+"_sub.pgm")
            os.system("opening  "+chemin_commun+"/Gnatho/"+i[:-4]+"_sub.pgm 3020el "+chemin_commun+"/Gnatho/"+i[:-4]+"_Opened.pgm")
            nbcomp=int(subprocess.check_output(["nbcomp",chemin_commun+"/Gnatho/"+i[:-4]+"_Opened.pgm","8","max"]))
        
          

    ########Detection de la composante la plus proche de l'oeil __________________________________
           

            os.system("explodecomp  "+chemin_commun+"/Gnatho/"+i[:-4]+"_Opened.pgm 8 max "+chemin_commun+"/Gnatho/"+i[:-4]+"_comp_")
            os.system("areaclosing  "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm 8 400 "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm ")
            os.system("areaopening  "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm 8 400 "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm ")

            compProche=0
            DistMin=1000

            if  os.path.exists(chemin_commun+"/oeil_files/CC_oeil_"+i[:-4]+".pgm")==False:
                os.system("volselnb "+chemin_commun+'/Gnatho/'+i[:-4]+"_Opened.pgm  8 1 "+chemin_commun+"/Gnatho/"+i[:-4]+"_comp_000"+str(compProche)+".pgm ")

            
            else:    
                for comp in range (1,nbcomp+1):
                    
        
                    EyeDist=float(subprocess.check_output(["distsets",chemin_commun+"/Gnatho/"+i[:-4]+"_comp_000"+str(comp)+".pgm",chemin_commun+"/oeil_files/CC_oeil_"+i[:-4]+".pgm"])[:-1])
                    if (EyeDist<125) and (EyeDist>20) and EyeDist<DistMin:
                    
                        DistMin=EyeDist    
                        compProche=comp
            

    ########Calcul Volume____________________________________________

            os.system("volume  "+chemin_commun+"/Gnatho/"+i[:-4]+"_SeuilOtsu.pgm  "+chemin_commun+"/Gnatho/"+i[:-4]+"_VolumeTot")    
            with open(chemin_commun+"/Gnatho/"+i[:-4]+"_VolumeTot") as f:
                lines = f.read().splitlines()
                vTot=float(lines[1][2:]) 

            if compProche!=0 or os.path.exists(chemin_commun+"/oeil_files/CC_oeil_"+i[:-4]+".pgm")==False:
                os.system("volume  "+chemin_commun+"/Gnatho/"+i[:-4]+"_comp_000"+str(compProche)+".pgm "  +chemin_commun+"/Gnatho/"+i[:-4]+"_Volume")
                os.system("surimp "+chemin_commun+'/pgm/rotation/'+i[:-4]+".pgm  "+chemin_commun+"/Gnatho/"+i[:-4]+"_comp_000"+str(compProche)+".pgm "+chemin_commun+"/DetectionGnatho/"+i[:-4]+"_SurimpGnatho")
                
                with open(chemin_commun+"/Gnatho/"+i[:-4]+"_Volume") as f:
                    lines = f.read().splitlines()
                    v=float(lines[1][2:]) 
            else:
                v=0
                    


    #######Resultat___________________________________________________
            """
            Choix entre résultat binaire ou quantitatif, à préciser dans le return
            """
            VolGnatho=0
            Gnathopodes=0
                
            if v==78336000 or v==0 :
                Gnathopodes=0
                VolGnatho=0    
                print(chemin_commun,i[:-4]," ",VolGnatho," ",Gnathopodes)    
            else:
                Gnathopodes=1
                VolGnatho=(v*100)/vTot
                print(chemin_commun,i[:-4]," ",VolGnatho," ",Gnathopodes)
        except:
            print("error image",i) 
    
        # return Gnathopodes    
        # return VolGnatho       


def Gnatho_exe():

    Gnatho(files_rfo, "femelles_oeufs")
    Gnatho(files_rfso, "femelles_sans_oeufs")
    Gnatho(files_rmg, "males_gnathopode")
    Gnatho(files_rmgne, "males_gnathopode_non_evident")

